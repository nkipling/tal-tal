# Tal Tal

[![Netlify Status](https://api.netlify.com/api/v1/badges/cfc099fe-4006-46a1-9fa4-96ae347cbc3c/deploy-status)](https://app.netlify.com/sites/taltal/deploys)

A small frontend app that helps manage your GitLab issues and keeps track of
your **async updates**.

|Main|Adding Update|Pinned Items|
|----|-------------|------------|
|![](images/main-screen-1.png)|![](images/main-screen-2.png)|![](images/pinned-items-1.png)|

Available at: https://taltal.netlify.com/

## Current Status

In `dev`. Async updates are tracked and can be added via the UI. Basic pinned
items support now working too.

## Planned Features

* ~~A quick status updater for when there are no real updates~~ Can now skip updates
* Better pinned item support to include merge requests and more
* Better pinned item notes, supporting of embedded todos, etc
* Custom async update templates
* ~~Drag and drop ordering~~ Mostly done
* "Minimal" view to hide even more issue detail
* Remove / adjust the auto refresh

## Want to help?

Sure. Start by cloning the repo and running `yarn install`. I suggest you also
create a `.env.development.local` file at the root directory with the following
content:

```
PORT=9999
client_id={your-oauth-client-id}
redirect_uri=http://localhost:9999/callback
```

You can grab your own OAuth client id by [following these instructions](https://docs.gitlab.com/ee/integration/oauth_provider.html#introduction-to-oauth).

Local development can then be started by running `yarn start`.
