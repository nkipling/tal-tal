import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import {
  Issue,
  IssueNote,
  IssueProject,
  PinnedItem,
  PinnedItemType,
  PinnedNote,
} from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';

type ItemType = {
  type: PinnedItemType;
  id: number;
};

type NotePayload = {
  note: PinnedNote;
  itemId: number;
};

type RemoveNotePayload = {
  noteId: string;
  itemId: number;
};

type ReorderPayload = {
  currentIndex: number;
  newIndex: number;
  categoryId?: string;
};

// === BASIC ACTIONS === //

const setGetIssueLoading = createAction('SET_PINNED_GET_ITEM_LOADING')<boolean>();
const addPinnedItem = createAction('ADD_PINNED_ITEM')<PinnedItem>();
const removePinnedItem = createAction('REMOVE_PINNED_ITEM')<number>();
const addNoteToPinnedItem = createAction('ADD_NOTE_TO_PINNED_ITEM')<NotePayload>();
const updatePinnedNote = createAction('UPDATE_PINNED_NOTE')<NotePayload>();
const removeNoteFromPinnedItem = createAction('REMOVE_NOTE_FROM_PINNED_ITEM')<RemoveNotePayload>();
const checkPinnedAssignees = createAction('CHECK_PINNED_ITEM_ASSIGNEES')<number>();
const updateIndexPosition = createAction('UPDATE_PINNED_INDEX_POSITION')<ReorderPayload>();
const addCategory = createAction('ADD_CATEGORY')<string>();
const setSelectedCategory = createAction('SET_SELECTED_CATEGORY')<string>();
const reset = createAction('PINNED_RESET')<void>();
const setupInitial = createAction('PINNED_SETUP')<void>();

export const pinnedActions = {
  setGetIssueLoading,
  addPinnedItem,
  removePinnedItem,
  addNoteToPinnedItem,
  updatePinnedNote,
  removeNoteFromPinnedItem,
  checkPinnedAssignees,
  updateIndexPosition,
  addCategory,
  setSelectedCategory,
  reset,
  setupInitial,
};

export type PinnedActions = ActionType<typeof pinnedActions>;

// === ASYNC ACTIONS === //

export const fetchItemToPin = (itemId: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state: AppState = getState();
    const { items = [] } = state.pinned;
    const { type, id } = getItemType(itemId);

    if (!isNaN(id) && !items.find(x => x.id === id)) {
      try {
        const selectedProject = state.issues.projects.find(x => x.selected);

        if (selectedProject) {
          dispatch(setGetIssueLoading(true));

          const fetchFunction = getFetchFunction(type);
          const item = await fetchFunction(selectedProject, id);

          dispatch(addPinnedItem(item));
          dispatch(checkPinnedAssignees(state.auth.user?.id as number));
        }
      } catch (ex) {
        iziToast.error({
          message: 'There was an error with the request.',
        });
      } finally {
        dispatch(setGetIssueLoading(false));
      }
    }
  };
};

const getItemType = (id: string): ItemType => {
  const charAZero = id.trim().charAt(0);
  const rest = parseInt(id.trim().substring(1));

  switch (charAZero) {
    case '&':
      return {
        type: PinnedItemType.EPIC,
        id: rest,
      };

    case '#':
      return {
        type: PinnedItemType.ISSUE,
        id: rest,
      };

    default:
      return {
        type: PinnedItemType.ISSUE,
        id: parseInt(id.trim()),
      };
  }
};

// TODO: Refine types
const getFetchFunction = (type: PinnedItemType): ((project: IssueProject, id: number) => any) => {
  switch (type) {
    case PinnedItemType.EPIC:
      return getEpicDetail;

    default:
      return getIssueDetail;
  }
};

const getIssueDetail = async (project: IssueProject, issueIid: number) => {
  const { data } = await axios.get<Issue>(`/projects/${project.id}/issues/${issueIid}`);

  const { data: discussionsData } = await axios.get<IssueNote[]>(
    `/projects/${project.id}/issues/${issueIid}/notes?order_by=updated_at`,
  );

  return {
    ...data,
    notes: discussionsData || [],
  };
};

const getEpicDetail = async (project: IssueProject, epicId: number) => {
  const { data } = await axios.get(`/groups/${project.groupId}/epics/${epicId}`);

  return data;
};
