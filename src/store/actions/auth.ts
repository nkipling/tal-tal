import ClientOAuth2 from 'client-oauth2';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import history from '../../history';
import { GitLabUser } from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';
import { issueActions } from './issues';
import { pinnedActions } from './pinned';

// Use OAuth to authenticate with GitLab
// TODO: Allow for custom GitLab instances and not just .com?
const oauth = new ClientOAuth2({
  clientId: process.env.REACT_APP_CLIENT_ID,
  authorizationUri: 'https://gitlab.com/oauth/authorize',
  redirectUri: process.env.REACT_APP_REDIRECT_URI,
  scopes: ['api', 'read_user', 'profile'],
});

// === BASIC ACTIONS === //

const setAccessToken = createAction('SET_ACCESS_TOKEN')<string | null>();
const setUser = createAction('SET_USER')<GitLabUser>();
const reset = createAction('AUTH_RESET')<void>();

export const authActions = {
  setAccessToken,
  setUser,
  reset,
};

export type AuthActions = ActionType<typeof authActions>;

// === ASYNC ACTIONS === //

// This doesn't really need to be an action but I'm leaving it here in case we
// ever want to attach more logic or change the auth method.
export const startLogin = () => {
  return async () => (window.location.href = oauth.token.getUri());
};

export const getCurrentUserDetails = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const { data } = await axios.get('/user');
      dispatch(setUser(data));
      dispatch(pinnedActions.setupInitial());
    } catch (error) {
      dispatch(setAccessToken(''));
      history.push('/');
    }
  };
};

export const signOut = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    dispatch(issueActions.reset());
    dispatch(pinnedActions.reset());
    dispatch(reset());

    history.push('/');
  };
};
