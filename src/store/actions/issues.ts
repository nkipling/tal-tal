import iziToast from 'izitoast';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { Issue, IssueNote, IssueProject, Label, workflowScope } from '../../models';
import axios from '../axios';
import { AppState } from '../reducers';

type AddIssueNote = {
  issueId: number;
  note: IssueNote;
};

type ReorderPayload = {
  currentIndex: number;
  newIndex: number;
};

type LabelsPayload = {
  projectId: number;
  labels: Label[];
};

type UpdateLabelsPayload = {
  issueId: number;
  labels: string[];
};

// === BASIC ACTIONS === //

const setGetIssueLoading = createAction('SET_GET_ISSUE_LOADING')<boolean>();
const setGetProjectLoading = createAction('SET_GET_PROJECT_LOADING')<boolean>();
const setIssueUpdatesLoading = createAction('SET_ISSUE_UPDATES_LOADING')<boolean>();
const setAsyncUpdateLoading = createAction('SET_ASYNC_UPDATE_LOADING')<boolean>();
const setLabelsLoading = createAction('SET_LABELS_LOADING')<boolean>();
const setLabelsForProject = createAction('SET_LABELS_FOR_PROJECT')<LabelsPayload>();
const updateIssueLabels = createAction('UPDATE_ISSUE_LABELS')<UpdateLabelsPayload>();
const setAllIssues = createAction('SET_ALL_ISSUES')<Issue[]>();
const addIssue = createAction('ADD_ISSUE')<Issue>();
const addProject = createAction('ADD_PROJECT')<IssueProject>();
const addNoteToIssue = createAction('ADD_NOTE_TO_ISSUE')<AddIssueNote>();
const skipIssueUpdate = createAction('SKIP_UPDATE')<number>();
const setSelectedProject = createAction('SET_SELECTED_PROJECT')<number>();
const removeIssue = createAction('REMOVE_ISSUE')<Issue>();
const updateIndexPosition = createAction('UPDATE_ISSUE_INDEX_POSITION')<ReorderPayload>();
const checkAssignees = createAction('CHECK_ASSIGNEES')<number>();
const buildMilestoneTabs = createAction('BUILD_MILESTONE_TABS')<void>();
const reset = createAction('ISSUES_RESET')<void>();

export const issueActions = {
  setGetIssueLoading,
  setGetProjectLoading,
  setIssueUpdatesLoading,
  setAsyncUpdateLoading,
  setLabelsLoading,
  setLabelsForProject,
  updateIssueLabels,
  setAllIssues,
  addIssue,
  addProject,
  addNoteToIssue,
  skipIssueUpdate,
  setSelectedProject,
  removeIssue,
  updateIndexPosition,
  checkAssignees,
  buildMilestoneTabs,
  reset,
};

export type IssueActions = ActionType<typeof issueActions>;

// === ASYNC ACTIONS === //

export const fetchIssueDetail = (issueId: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state: AppState = getState();
    const { allIssues } = state.issues;
    const issueIntId = parseInt(issueId);

    if (!isNaN(issueIntId) && !allIssues.find(x => x.iid === issueIntId)) {
      try {
        const selectedProject = state.issues.projects.find(x => x.selected);

        if (selectedProject) {
          dispatch(setGetIssueLoading(true));

          const issue = await getIssueDetail(selectedProject.id, issueIntId);

          dispatch(addIssue(issue));
          dispatch(checkAssignees(state.auth.user?.id as number));
          dispatch(buildMilestoneTabs());
        }
      } catch (ex) {
        iziToast.error({
          message: 'There was an error fetching the issue detail.',
        });
      } finally {
        dispatch(setGetIssueLoading(false));
      }
    }
  };
};

export const fetchProjectDetails = (projectId: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state: AppState = getState();
    const { projects } = state.issues;
    const projectIntId = parseInt(projectId);

    if (!isNaN(projectIntId) && !projects.find(x => x.id === projectIntId)) {
      try {
        dispatch(setGetProjectLoading(true));
        const { data } = await axios.get(`/projects/${projectIntId}`);

        const project: IssueProject = {
          id: projectIntId,
          name: data.name,
          selected: false,
          groupId: data.namespace?.id,
        };

        dispatch(addProject(project));
      } catch (ex) {
        console.error(ex);

        iziToast.error({
          message: 'There was an error fetching the project details.',
        });
      } finally {
        dispatch(setGetProjectLoading(false));
      }
    }
  };
};

export const updateIssues = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    dispatch(setIssueUpdatesLoading(true));

    const state: AppState = getState();
    const { allIssues } = state.issues;
    const issuesToUpdate = [];

    for (let i of allIssues) {
      issuesToUpdate.push(getIssueDetail(i.project_id, i.iid));
    }

    Promise.all(issuesToUpdate)
      .then(updatedIssues => {
        dispatch(setAllIssues(updatedIssues));
        dispatch(checkAssignees(state.auth.user?.id as number));
        dispatch(buildMilestoneTabs());
        dispatch(setIssueUpdatesLoading(false));
      })
      .catch(ex => {
        console.error(ex);

        iziToast.error({
          message: 'There was an error updating the issues.',
        });
      });
  };
};

const getIssueDetail = async (projectId: number, issueIid: number) => {
  const { data } = await axios.get<Issue>(`/projects/${projectId}/issues/${issueIid}`);

  const { data: discussionsData } = await axios.get<IssueNote[]>(
    `/projects/${projectId}/issues/${issueIid}/notes?order_by=updated_at`,
  );

  return {
    ...data,
    notes: discussionsData || [],
  };
};

export const sendAsyncUpdate = (update: string, issue: Issue) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      dispatch(setAsyncUpdateLoading(true));

      const { data } = await axios.post(
        `/projects/${issue.project_id}/issues/${issue.iid}/notes`,
        null,
        { params: { body: update } },
      );

      dispatch(addNoteToIssue({ issueId: issue.id, note: data }));

      iziToast.success({
        message: 'Async update successfully sent.',
      });
    } catch (ex) {
      console.error(ex);

      iziToast.error({
        message: 'There was an error sending the update.',
      });
    } finally {
      dispatch(setAsyncUpdateLoading(false));
    }
  };
};

/**
 * This would be nice to use except there doesn't appear to be any way to filter
 * the results and there are 54 pages for the GitLab project...
 * @param projectId Project id to fetch labels
 */
export const getProjectLabels = (projectId: number) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      dispatch(setLabelsLoading(true));

      const { data: labels } = await axios.get(`/projects/${projectId}/labels`);

      dispatch(setLabelsForProject({ projectId, labels }));
    } catch (ex) {
      console.error(ex);

      iziToast.error({
        message: 'There was an error fetching project labels.',
      });
    } finally {
      dispatch(setLabelsLoading(false));
    }
  };
};

export const changeWorkflowLabel = (issue: Issue, newLabel: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      dispatch(setLabelsLoading(true));

      const {
        data: { labels },
      } = await axios.get<Issue>(`/projects/${issue.project_id}/issues/${issue.iid}`);
      const workflowIndex = labels.findIndex(x => x.includes(workflowScope));

      if (workflowIndex > -1) {
        labels.splice(workflowIndex, 1);
      }

      labels.push(newLabel);

      const { data } = await axios.put(`/projects/${issue.project_id}/issues/${issue.iid}`, {
        labels,
      });

      dispatch(updateIssueLabels({ issueId: issue.id, labels: data.labels }));

      iziToast.success({
        message: 'Workflow label updated.',
      });
    } catch (ex) {
      console.error(ex);

      iziToast.error({
        message: 'There was an error applying the label.',
      });
    } finally {
      dispatch(setLabelsLoading(false));
    }
  };
};

// TODO: I don't think we're going to need this?
// export const fetchAllIssues = () => {
//   return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
//     try {
//       const { data } = await axios.get(`/issues?state=opened&order_by=updated_at`);

//       dispatch(setAllIssues(data));
//     } catch (ex) {
//       // TODO: Fix this up
//       console.error(ex);
//     }
//   };
// };
