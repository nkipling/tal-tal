import moment from 'moment';
import { MilestoneTab, PinnedItemContainer, uncategorised } from '../models';
import { AppState } from './reducers';

// === Auth === //

export const getAccessToken = (store: AppState) => {
  return store.auth.accessToken;
};

export const getCurrentUser = (store: AppState) => {
  return store.auth.user;
};

// === Issues === //

export const getAllIssues = (store: AppState) => {
  return store.issues.allIssues || [];
};

export const getAllIssueProjects = (store: AppState) => {
  return store.issues.projects || [];
};

export const getIssueLoading = (store: AppState) => {
  return store.issues.getIssueLoading || store.pinned.getIssueLoading;
};

export const getProjectLoading = (store: AppState) => {
  return store.issues.getProjectLoading;
};

export const getIssuesUpdatesLoading = (store: AppState) => {
  return store.issues.issuesUpdatesLoading;
};

export const getAsyncUpdateLoading = (store: AppState) => {
  return store.issues.asyncUpdateLoading;
};

export const getLabelsLoading = (store: AppState) => {
  return store.issues.labelsLoading;
};

export const labelsNeedUpdating = (store: AppState, projectId: number) => {
  if (store.issues.lastLabelFetch) {
    const projectLastFetch = store.issues.lastLabelFetch[projectId];

    if (!projectLastFetch) {
      return true;
    }

    if (projectLastFetch) {
      if (moment(projectLastFetch).isBefore(moment().subtract(1, 'day'))) {
        return true;
      }
    }

    return false;
  }

  return true;
};

export const getSelectedProject = (store: AppState) => {
  return store.issues.projects.find(x => x.selected);
};

export const getLastAsyncUpdate = (store: AppState, issueId: number) => {
  const issue = store.issues.allIssues.find(x => x.id === issueId);

  if (issue && issue.notes) {
    for (let i = 0; i < issue.notes.length; i++) {
      const n = issue.notes[i];

      if (n.body.toLowerCase().includes('async issue update')) {
        return n;
      }
    }
  }

  return null;
};

export const isIssueSkipped = (store: AppState, issueId: number) => {
  if (!store.issues.skippedIssues) {
    return false;
  }

  const skippedTs = store.issues.skippedIssues[issueId];

  if (skippedTs) {
    return moment(skippedTs).isSame(moment(), 'date');
  }

  return false;
};

export const getMilestoneTabs = (store: AppState) => {
  const { milestones } = store.issues;

  if (milestones) {
    const newMilestones: MilestoneTab[] = Object.keys(milestones)
      .map(x => ({
        label: x === 'rest' ? 'None' : x,
        issues: milestones[x],
        count: milestones[x].length,
      }))
      .sort((a, b) =>
        a.label.toLowerCase() > b.label.toLowerCase()
          ? 1
          : b.label.toLowerCase() > a.label.toLowerCase()
          ? -1
          : 0,
      );

    const backlogIndex = newMilestones.findIndex(x => x.label.toLowerCase() === 'backlog');

    // Only reorder if backlog isn't the first tab
    if (backlogIndex > 0) {
      newMilestones.splice(0, 0, ...newMilestones.splice(backlogIndex, 1));
    }

    return newMilestones.filter(x => x.count > 0);
  }

  return null;
};

// === Pinned === //

export const getPinnedItems = (store: AppState) => {
  const noCategory =
    (store.pinned.categories || []).find(x => x.name === 'Uncategorised') || uncategorised;
  const pinnedItems: PinnedItemContainer[] = [{ category: noCategory, items: [] }];

  for (let i = 0; i < store.pinned.items.length; i++) {
    const item = store.pinned.items[i];

    if (!item) {
      continue;
    }

    if (item.categoryId) {
      const category = pinnedItems.find(c => c.category.id === item.categoryId);
      const categoryToAdd = store.pinned.categories.find(c => c.id === item.categoryId);

      if (category) {
        category.items.push(item);
        continue;
      } else if (categoryToAdd) {
        pinnedItems.push({
          category: categoryToAdd,
          items: [item],
        });
        continue;
      }
    } else {
      // Uncategorised
      pinnedItems[0].items.push(item);
    }
  }

  return pinnedItems || [];
};

export const getPinnedCategories = (store: AppState) => {
  return store.pinned.categories || [];
};

export const getSelectedCategory = (store: AppState) => {
  return (store.pinned.categories || []).find(x => x.selected);
};
