import shortid from 'shortid';
import { createReducer } from 'typesafe-actions';
import { Category, Issue, PinnedItem, uncategorised } from '../../models';
import { PinnedActions, pinnedActions } from '../actions/pinned';

export interface PinnedState {
  getIssueLoading: boolean;
  items: PinnedItem[];
  categories: Category[];
}

const initialState: PinnedState = {
  getIssueLoading: false,
  items: [],
  categories: [uncategorised],
};

export default createReducer<PinnedState, PinnedActions>(initialState)
  .handleAction(pinnedActions.setGetIssueLoading, (state, action) => ({
    ...state,
    getIssueLoading: action.payload,
  }))
  .handleAction(pinnedActions.addPinnedItem, (state, action) => {
    const selectedCategory = state.categories.find(x => x.selected);

    if (selectedCategory) {
      action.payload.categoryId = selectedCategory.id;
    }

    return {
      ...state,
      items: [...state.items, action.payload],
    };
  })
  .handleAction(pinnedActions.removePinnedItem, (state, action) => {
    const itemIndex = state.items.findIndex(x => x.id === action.payload);

    if (itemIndex > -1) {
      state.items.splice(itemIndex, 1);
    }

    return {
      ...state,
      items: [...state.items],
    };
  })
  .handleAction(pinnedActions.addNoteToPinnedItem, (state, action) => {
    const item = state.items.find(x => x.id === action.payload.itemId);

    if (item) {
      if (item.talUserNotes) {
        item.talUserNotes.push(action.payload.note);
      } else {
        item.talUserNotes = [action.payload.note];
      }
    }

    return {
      ...state,
      items: [...state.items],
    };
  })
  .handleAction(pinnedActions.updatePinnedNote, (state, action) => {
    const item = state.items.find(x => x.id === action.payload.itemId);

    if (item) {
      const note = item.talUserNotes?.find(x => x.id === action.payload.note.id);

      if (note) {
        note.content = action.payload.note.content;
      }
    }

    return {
      ...state,
      items: [...state.items],
    };
  })
  .handleAction(pinnedActions.removeNoteFromPinnedItem, (state, action) => {
    const item = state.items.find(x => x.id === action.payload.itemId);

    if (item && item.talUserNotes) {
      const noteIndex = item.talUserNotes.findIndex(x => x.id === action.payload.noteId);

      if (noteIndex > -1) {
        item.talUserNotes.splice(noteIndex, 1);
      }
    }

    return {
      ...state,
      items: [...state.items],
    };
  })
  .handleAction(pinnedActions.checkPinnedAssignees, (state, action) => {
    state.items.forEach(x => checkIssueAssignees(x, action.payload));

    return {
      ...state,
      items: [...state.items],
    };
  })
  .handleAction(pinnedActions.updateIndexPosition, (state, action) => {
    const { currentIndex, newIndex, categoryId } = action.payload;

    const [category, rest] = state.items.reduce<[PinnedItem[], PinnedItem[]]>(
      (acc, cur) => {
        const [category, rest] = acc;

        if (cur && cur.categoryId === categoryId) {
          category.push(cur);
        } else if (cur) {
          rest.push(cur);
        }

        return [category, rest];
      },
      [[], []],
    );

    // Swap item positions
    category[newIndex] = category.splice(currentIndex, 1, category[newIndex])[0];

    return {
      ...state,
      items: [...rest, ...category],
    };
  })
  .handleAction(pinnedActions.addCategory, (state, action) => {
    const { categories = [] } = state;

    if (!categories.find(x => x.name.toLowerCase() === action.payload.toLowerCase())) {
      categories.forEach(x => (x.selected = false));

      if (action.payload === 'Uncategorised') {
        categories.push(uncategorised);
      } else {
        categories.push({
          id: shortid.generate(),
          name: action.payload,
          selected: true,
        });
      }

      categories.sort((a, b) => a.name.localeCompare(b.name));

      return {
        ...state,
        categories,
      };
    }

    return state;
  })
  .handleAction(pinnedActions.setSelectedCategory, (state, action) => {
    state.categories.forEach(x => (x.selected = false));
    const toSelect = state.categories.find(x => x.id === action.payload);

    if (toSelect) {
      toSelect.selected = true;
    }

    return {
      ...state,
      categories: [...state.categories],
    };
  })
  .handleAction(pinnedActions.reset, (state, action) => initialState)
  .handleAction(pinnedActions.setupInitial, (state, action) => (state ? state : initialState));

const checkIssueAssignees = (x: Issue, userId: number) => {
  if (x.assignees && x.assignees.length) {
    if (x.assignees.find(a => a.id === userId)) {
      x.does_not_include_current_user = false;
    } else {
      x.does_not_include_current_user = true;
    }
  } else {
    x.does_not_include_current_user = true;
  }
};
