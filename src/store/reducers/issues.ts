import moment from 'moment';
import { createReducer } from 'typesafe-actions';
import { Issue, IssueNote, IssueProject } from '../../models';
import { Label } from '../../models/label';
import { issueActions, IssueActions } from '../actions/issues';

type MilestoneTabs = {
  [key: string]: number[];
};

export interface IssuesState {
  getIssueLoading: boolean;
  getProjectLoading: boolean;
  issuesUpdatesLoading: boolean;
  asyncUpdateLoading: boolean;
  labelsLoading: boolean;
  lastLabelFetch: {
    [key: number]: number;
  };
  labels: {
    [key: number]: Label[];
  };
  allIssues: Issue[];
  skippedIssues: {
    [key: number]: number;
  };
  projects: IssueProject[];
  milestones: MilestoneTabs;
}

const initialState: IssuesState = {
  getIssueLoading: false,
  getProjectLoading: false,
  issuesUpdatesLoading: false,
  asyncUpdateLoading: false,
  labelsLoading: false,
  lastLabelFetch: {},
  labels: {},
  allIssues: [],
  skippedIssues: {},
  projects: [{ name: 'GitLab', id: 278964, selected: true, groupId: 9970 }],
  milestones: {},
};

export default createReducer<IssuesState, IssueActions>(initialState)
  .handleAction(issueActions.setGetIssueLoading, (state, action) => ({
    ...state,
    getIssueLoading: action.payload,
  }))
  .handleAction(issueActions.setGetProjectLoading, (state, action) => ({
    ...state,
    getProjectLoading: action.payload,
  }))
  .handleAction(issueActions.setIssueUpdatesLoading, (state, action) => ({
    ...state,
    issuesUpdatesLoading: action.payload,
  }))
  .handleAction(issueActions.setAsyncUpdateLoading, (state, action) => ({
    ...state,
    asyncUpdateLoading: action.payload,
  }))
  .handleAction(issueActions.setLabelsLoading, (state, action) => ({
    ...state,
    labelsLoading: action.payload,
  }))
  .handleAction(issueActions.setLabelsForProject, (state, action) => {
    const { projectId, labels: labelsResult } = action.payload;
    const ts = new Date().valueOf();

    const { labels = {}, lastLabelFetch = {} } = state;

    labels[projectId] = labelsResult;
    lastLabelFetch[projectId] = ts;

    return {
      ...state,
      labels,
      lastLabelFetch,
    };
  })
  .handleAction(issueActions.updateIssueLabels, (state, action) => {
    const { issueId, labels } = action.payload;
    const { allIssues } = state;

    const issue = state.allIssues.find(x => x.id === issueId);

    if (issue) {
      issue.labels = labels;
    }

    return {
      ...state,
      ...allIssues,
    };
  })
  .handleAction(issueActions.setAllIssues, (state, action) => {
    const { payload: issues } = action;
    const newIssues = [];

    for (let i of issues) {
      newIssues.push(processIssueNotes(i));
    }

    return {
      ...state,
      allIssues: newIssues,
    };
  })
  .handleAction(issueActions.addIssue, (state, action) => {
    const issue = processIssueNotes(action.payload);

    return {
      ...state,
      allIssues: [...state.allIssues, issue],
    };
  })
  .handleAction(issueActions.addProject, (state, action) => {
    const [gitlab, ...rest] = state.projects;

    rest.push(action.payload);
    rest.sort((a, b) =>
      a.name.toLowerCase() > b.name.toLowerCase()
        ? 1
        : b.name.toLowerCase() > a.name.toLowerCase()
        ? -1
        : 0,
    );

    return {
      ...state,
      projects: [gitlab, ...rest],
    };
  })
  .handleAction(issueActions.addNoteToIssue, (state, action) => {
    const issue = state.allIssues.find(x => x.id === action.payload.issueId);

    if (issue) {
      const note = [action.payload.note];

      if (issue.notes) {
        issue.notes = [...note, ...(issue.notes as IssueNote[])];
      } else {
        issue.notes = note;
      }

      processIssueNotes(issue);

      return {
        ...state,
        allIssues: [...state.allIssues],
      };
    }

    return state;
  })
  .handleAction(issueActions.skipIssueUpdate, (state, action) => {
    const issue = state.allIssues.find(x => x.id === action.payload);
    const { skippedIssues = {} } = state;

    if (issue) {
      skippedIssues[issue.id] = new Date().valueOf();
    }

    return {
      ...state,
      skippedIssues,
    };
  })
  .handleAction(issueActions.setSelectedProject, (state, action) => {
    state.projects.forEach(x => (x.selected = false));
    state.projects[action.payload].selected = true;

    return {
      ...state,
      projects: [...state.projects],
    };
  })
  .handleAction(issueActions.removeIssue, (state, action) => {
    const idx = state.allIssues.findIndex(x => x.id === action.payload.id);
    state.allIssues.splice(idx, 1);

    return {
      ...state,
      allIssues: [...state.allIssues],
    };
  })
  .handleAction(issueActions.updateIndexPosition, (state, action) => {
    const { currentIndex, newIndex } = action.payload;
    const { allIssues } = state;

    // Swap item positions
    allIssues[newIndex] = allIssues.splice(currentIndex, 1, allIssues[newIndex])[0];

    return {
      ...state,
      allIssues: [...allIssues],
    };
  })
  .handleAction(issueActions.checkAssignees, (state, action) => {
    state.allIssues.forEach(x => {
      if (x.assignees && x.assignees.length) {
        if (x.assignees.find(a => a.id === action.payload)) {
          x.does_not_include_current_user = false;
        } else {
          x.does_not_include_current_user = true;
        }
      } else {
        x.does_not_include_current_user = true;
      }
    });

    return {
      ...state,
      allIssues: [...state.allIssues],
    };
  })
  .handleAction(issueActions.buildMilestoneTabs, (state, action) => {
    const milestones: MilestoneTabs = { rest: [] };

    state.allIssues.forEach(x => {
      if (x.milestone) {
        const { title } = x.milestone;

        if (milestones[title]) {
          milestones[title].push(x.iid);
        } else {
          milestones[title] = [x.iid];
        }
      } else {
        milestones.rest.push(x.iid);
      }
    });

    return {
      ...state,
      milestones,
    };
  })
  .handleAction(issueActions.reset, (state, action) => initialState);

/**
 * Processes the issue notes to determine if an update is required for today.
 * @param issue
 */
const processIssueNotes = (issue: Issue) => {
  const today = moment();

  if (issue.notes && issue.notes.length) {
    // Notes are passed in descending order, so a for loop is fine here.
    // Alternatively, we could just find the last 'async issue update' note and
    // check which date that was made on - might be worth switching to this.
    for (let n of issue.notes) {
      if (isNoteAsyncUpdate(n)) {
        const date = moment(n.created_at);

        if (today.isAfter(date, 'day')) {
          // We don't have an update for today
          issue.updated_completed = false;
          break;
        } else if (today.isSame(date, 'day')) {
          // We do have an update for today
          issue.updated_completed = true;
          break;
        }
      }
    }
  }

  // Explicitly set update_completed to false if undefined
  issue.updated_completed = issue.updated_completed ?? false;

  return issue;
};

const isNoteAsyncUpdate = (note: IssueNote) => {
  const possible = ['async issue update', 'async update', 'issue update'];

  for (let p of possible) {
    if (note.body.toLowerCase().includes(p)) {
      return true;
    }
  }

  return false;
};
