import { combineReducers } from 'redux';
import auth, { AuthState } from './auth';
import issues, { IssuesState } from './issues';
import pinned, { PinnedState } from './pinned';

export interface AppState {
  auth: AuthState;
  issues: IssuesState;
  pinned: PinnedState;
}

export default combineReducers({ auth, issues, pinned });
