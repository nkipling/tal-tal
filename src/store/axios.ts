import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
});

(window as any).ax = instance;

export default instance;
