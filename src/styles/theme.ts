export interface ThemeInterface {
  color1: string;
  color2: string;
  color3: string;
  color4: string;
  color5: string;
  color6: string;
}

export type ThemeType = {
  theme: ThemeInterface;
};

export const defaultTheme = {
  color1: '#fca121', // Light Orange
  color2: '#fc6d26', // Orange
  color3: '#db3b21', // Red Orange
  color4: '#6e49ca', // Light Purple
  color5: '#380d75', // Purple
  color6: '#2e2e2e', // Dark Grey
};
