import React from 'react';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import { Provider, useSelector } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { PrivateRoute } from './components/private-route';
import history from './history';
import { persistor, store } from './store';
import axios from './store/axios';
import { getAccessToken } from './store/selectors';
import { CallbackView } from './views/callback';
import { IssuesView } from './views/issues';
import { PinnedView } from './views/pinned';
import { WelcomeView } from './views/welcome';

const InnerApp = () => {
  const accessToken = useSelector(getAccessToken);
  axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken || ''}`;

  return (
    <Router history={history}>
      <DndProvider backend={Backend}>
        <Switch>
          <PrivateRoute exact path="/">
            <Route component={IssuesView} />
          </PrivateRoute>

          <PrivateRoute exact path="/pinned">
            <Route component={PinnedView} />
          </PrivateRoute>

          <Route exact path="/welcome" component={WelcomeView} />
          <Route exact path="/callback" component={CallbackView} />
        </Switch>
      </DndProvider>
    </Router>
  );
};

export const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <InnerApp />
      </PersistGate>
    </Provider>
  );
};
