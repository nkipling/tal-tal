import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { startLogin } from '../store/actions/auth';

export const WelcomeView: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = 'Welcome to Tal Tal';
  }, []);

  const loginClick = () => {
    dispatch(startLogin());
  };

  return (
    <section className="section full-height is-flex">
      <div className="columns is-flex flex-1 is-vcentered is-centered">
        <div className="column is-half">
          <div className="box has-text-centered">
            <h1 className="title is-2">Welcome to Tal Tal</h1>

            <div className="content">
              <p>Package async updates made easy. Maybe.</p>
            </div>

            <button className="button is-primary" onClick={loginClick}>
              Login with GitLab
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};
