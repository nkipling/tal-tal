import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { authActions } from '../store/actions/auth';

export const CallbackView: React.FC = () => {
  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();

  if (location && location.hash) {
    const params = new URLSearchParams(location.hash);
    const accessToken = params.get('#access_token');

    if (accessToken) {
      dispatch(authActions.setAccessToken(accessToken));
      history.push('/');
      return null;
    }
  }

  history.push('welcome');

  return null;
};
