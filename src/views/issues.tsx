import cn from 'classnames';
import moment from 'moment';
import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCountdownTimer } from 'use-countdown-timer';
import { AddIssue } from '../components/add-issue';
import { HeaderBar } from '../components/header-bar';
import { IssuesList } from '../components/issues/issues-list';
import { IssueFilter } from '../models';
import { getCurrentUserDetails } from '../store/actions/auth';
import { updateIssues } from '../store/actions/issues';
import { getIssuesUpdatesLoading, getMilestoneTabs } from '../store/selectors';

export const IssuesView: React.FC = () => {
  const dispatch = useDispatch();
  const issuesLoading = useSelector(getIssuesUpdatesLoading);
  const milestoneTabs = useSelector(getMilestoneTabs);

  const [filter, setFilter] = useState(IssueFilter.ALL);
  const [activeTab, setActiveTab] = useState(0);

  useEffect(() => {
    document.title = 'Issues | Tal Tal';

    dispatch(getCurrentUserDetails());
  }, [dispatch]);

  const refreshIssues = () => {
    if (!issuesLoading) {
      reset();
      dispatch(updateIssues());
    }
  };

  const onFilterChange = (e: FormEvent<HTMLSelectElement>) => {
    setFilter(e.currentTarget.value as IssueFilter);
  };

  const onTabClick = (e: React.MouseEvent<HTMLAnchorElement>, index: number) => {
    e.preventDefault();
    setActiveTab(index);
  };

  const onCountdownExpire = () => {
    refreshIssues();
  };

  const { countdown, start, reset } = useCountdownTimer({
    timer: 1000 * 60 * 30,
    autostart: true,
    onExpire: onCountdownExpire,
  });
  const duration = moment.duration(countdown);

  useEffect(() => {
    if (issuesLoading === false) {
      start();
    }
  }, [issuesLoading, start]);

  return (
    <div>
      <HeaderBar />

      <section className="section">
        <div className="container">
          <div className="level is-mobile">
            <div className="level-left">
              <p className="title">Issues</p>
            </div>

            <div className="level-right">
              <div className="has-margin-right-5">
                <p className="is-size-7 fixed-width-number">
                  Next refresh: {moment.utc(duration.asMilliseconds()).format('mm:ss')}
                </p>
              </div>
              <button
                className="button has-tooltip-left is-small"
                onClick={refreshIssues}
                data-tooltip="Refresh data for all issues now"
              >
                <i className={cn('fas', 'fa-sync-alt', { 'fa-spin': issuesLoading })}></i>
              </button>
            </div>
          </div>

          <div className="level">
            <div className="level-left">
              <AddIssue />
            </div>

            <div className="level-right">
              <p className="is-size-7">Show</p>
              &nbsp;
              <div className="field has-addons is-expanded">
                <div className="control">
                  <div className="select is-small">
                    <select onChange={onFilterChange} defaultValue={filter}>
                      <option value={IssueFilter.ALL}>All</option>
                      <option value={IssueFilter.UPDATE}>Requiring update</option>
                      <option value={IssueFilter.USER}>Assigned to you</option>
                      <option value={IssueFilter.ELSE}>Not assigned to you</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {milestoneTabs && (
            <div className="columns">
              <div className="column">
                <div className="tabs">
                  <ul>
                    {milestoneTabs.map((x, index) => (
                      <li className={cn({ 'is-active': index === activeTab })}>
                        <a href="#1" onClick={e => onTabClick(e, index)}>
                          {x.label}

                          <span
                            className={cn('tab-badge', 'has-badge-inline', 'has-badge-info')}
                            data-badge={x.count}
                          />
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          )}

          <div className="columns">
            <div className="column">
              <IssuesList
                filter={filter}
                milestone={milestoneTabs ? milestoneTabs[activeTab] : null}
              />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
