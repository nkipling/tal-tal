import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddIssue } from '../components/add-issue';
import { HeaderBar } from '../components/header-bar';
import { Modal } from '../components/modal';
import { PinnedList } from '../components/pinned/pinned-list';
import { Category, IssueType } from '../models';
import { getPinnedCategories } from '../store/selectors';

export const PinnedView: React.FC = () => {
  const dispatch = useDispatch();
  const categories = useSelector(getPinnedCategories);
  const [filter, setFilter] = useState<Category | null>(null);
  const [showHelpModal, setShowHelpModal] = useState(false);

  useEffect(() => {
    document.title = 'Pinned | Tal Tal';
  }, [dispatch]);

  const onFilterChange = (e: FormEvent<HTMLSelectElement>) => {
    const category = categories.find(x => x.id === e.currentTarget.value);

    if (category) {
      setFilter(category);
    } else {
      setFilter(null);
    }
  };

  const openHelpModal = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    setShowHelpModal(true);
  };

  return (
    <div>
      <HeaderBar />

      <section className="section">
        <div className="container">
          <div className="level is-mobile">
            <div className="level-left">
              <p className="title">
                Pinned&nbsp;
                <a href="#1" className="is-size-6" onClick={openHelpModal}>
                  <i className="fas fa-question-circle"></i>
                </a>
              </p>
            </div>
          </div>

          <div className="level">
            <div className="level-left">
              <AddIssue issueType={IssueType.PINNED} showPinnedType={true} />
            </div>

            <div className="level-right">
              <p className="is-size-7">Show</p>
              &nbsp;
              <div className="field has-addons is-expanded">
                <div className="control">
                  <div className="select is-small">
                    <select onChange={onFilterChange} defaultValue="all" style={{ maxWidth: 120 }}>
                      <option value="all">All</option>
                      {categories.map(x => (
                        <option key={x.id} value={x.id}>
                          {x.name}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="columns">
            <div className="column">
              <PinnedList filter={filter} />
            </div>
          </div>
        </div>
      </section>

      <Modal
        open={showHelpModal}
        title="Pinned Items"
        buttons={[{ label: 'Okay', classes: ['is-link'], onClick: () => setShowHelpModal(false) }]}
      >
        <p>
          This can be used as a personal list to pin items to keep track of. Right now, it supports
          pinning issues (by default) and epics. Issues can be entered as just an id number or
          prefixed with a <code>#</code>. Epics must be prefixed with an <code>&amp;</code>, e.g.{' '}
          <code>&amp;2480</code>.
        </p>

        <p>
          Once you have pinned an item, you can add your own notes. These are stored locally and are
          not sent to GitLab, etc.
        </p>
      </Modal>
    </div>
  );
};
