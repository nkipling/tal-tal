import { Category } from './category';
import { Issue } from './issue';

export interface PinnedItemContainer {
  category: Category;
  items: PinnedItem[];
}

export interface PinnedItem extends Issue {
  talUserNotes?: PinnedNote[];
  categoryId?: string;
}

export interface PinnedNote {
  id: string;
  content: string;
}

export enum PinnedItemType {
  EPIC = 'epic',
  ISSUE = 'issue',
}
