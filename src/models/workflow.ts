export const workflowScope = 'workflow::';

export const WorkflowStatus = [
  'In dev',
  'In review',
  'blocked',
  'canary',
  'design',
  'issue reviewed',
  'needs issue review',
  'planning breakdown',
  'problem validation',
  'production',
  'ready for development',
  'ready for review',
  'scheduling',
  'solution validation',
  'staging',
  'start',
  'validation backlog',
  'verification',
];
