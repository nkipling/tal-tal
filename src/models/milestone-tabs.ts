export interface MilestoneTab {
  label: string;
  issues: number[];
  count: number;
}
