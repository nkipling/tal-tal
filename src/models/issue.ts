import { GitLabUser } from './user';

export interface Issue {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: 'opened';
  created_at: string;
  updated_at: string;
  closed_at: string | null;
  closed_by: string | null;
  labels: string[];
  milestone: Milestone | null;
  assignees: GitLabUser[];
  author: GitLabUser;
  assignee: GitLabUser | null;
  user_notes_count: number;
  merge_requests_count: number;
  upvotes: number;
  downvotes: number;
  due_date: string | null;
  confidental: boolean;
  discussion_locked: boolean;
  web_url: string;
  time_stats: {
    time_estimate: number;
    total_time_spent: number;
    human_time_estimate: string | null;
    human_total_time_spent: string | null;
  };
  task_completion_status: {
    count: number;
    completed_count: number;
  };
  has_tasks: boolean;
  _links: {
    self: string;
    notes: string;
    award_emoji: string;
    project: string;
  };
  references: Reference;
  moved_to_id: number | null;
  weight: number | null;
  epic_iid: number | null;
  epic: EpicStub | null;

  // Custom props
  updated_completed?: boolean;
  notes?: IssueNote[];
  does_not_include_current_user?: boolean;
}

export interface EpicStub {
  id: number;
  iid: number;
  title: string;
  url: string;
  group_id: number;
  human_readable_end_data: string;
  human_readable_timestamp: string;
}

export interface Milestone {
  id: number;
  iid: number;
  group_id: number;
  title: string;
  description: string;
  state: 'active';
  created_at: string;
  updated_at: string;
  due_date: string | null;
  start_date: string | null;
  web_url: string;
}

export interface IssueProject {
  name: string;
  id: number;
  selected: boolean;
  groupId: number;
}

export interface IssueNote {
  id: number;
  type: 'DiscussionNote' | null;
  body: string;
  attachment: null;
  author: GitLabUser;
  created_at: string;
  updated_at: string;
  system: boolean;
  noteable_id: number;
  noteable_type: 'Issue';
  resolvable: boolean;
  noteable_iid: number;
}

export interface Reference {
  short: string;
  relative: string;
  full: string;
}

export enum IssueFilter {
  ALL = 'all',
  UPDATE = 'update',
  USER = 'user',
  ELSE = 'else',
}

export enum IssueType {
  ASYNC = 'async',
  PINNED = 'pinned',
}
