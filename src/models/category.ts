import shortid from 'shortid';

export interface Category {
  id: string;
  name: string;
  selected?: boolean;
}

export const uncategorised: Category = {
  id: shortid.generate(),
  name: 'Uncategorised',
  selected: true,
};
