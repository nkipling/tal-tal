import { PinnedItemType, Reference } from './models';

export const projectUrlFromReference = (reference: Reference) => {
  const uri = reference.full.substring(0, reference.full.lastIndexOf('#'));

  return `https://gitlab.com/${uri}`;
};

export const workOutType = (reference: Reference) => {
  if (reference.short.includes('#')) {
    return PinnedItemType.ISSUE;
  }

  if (reference.short.includes('&')) {
    return PinnedItemType.EPIC;
  }
};
