import cn from 'classnames';
import moment from 'moment';
import React, { useState } from 'react';
import * as Showdown from 'showdown';
import { IssueNote } from '../../models';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true,
});

interface Props {
  note: IssueNote;
  url: string;
  avatar?: string;
}

export const AsyncUpdate: React.FC<Props> = ({ note, url, avatar }) => {
  const parsedMarkdown = converter.makeHtml(note.body);
  const externalUrl = `${url}#note_${note.id}`;
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpenClick = () => {
    setIsOpen(!isOpen);
  };

  return (
    <article className="media has-margin-top-20">
      <div className="media-content async-update">
        <div
          className={cn('level', 'is-size-7', 'is-flex', {
            'has-margin-bottom-0': !isOpen,
            'has-margin-bottom-10': isOpen,
          })}
        >
          <div className="level-item is-narrow">
            <p className="image is-32x32">
              <img className="is-rounded" src={avatar} alt="User Avatar" />
            </p>
          </div>

          <div className="level-item has-margin-left-10 is-narrow">
            <p>
              Last update by: <strong>{note.author.name}</strong>{' '}
              {moment(note.created_at).fromNow()}.
              <a href={externalUrl} className="icon" target="_blank" rel="noopener noreferrer">
                <i className="fas fa-external-link-alt"></i>
              </a>
            </p>
            <button className="button is-white is-small" onClick={toggleOpenClick}>
              <span className="icon is-small">
                <i
                  className={cn('fas', { 'fa-chevron-down': !isOpen, 'fa-chevron-up': isOpen })}
                ></i>
              </span>
            </button>
          </div>
        </div>

        <div className="content is-size-7 has-margin-left-45 is-relative">
          <div className={cn({ 'is-open': isOpen, 'is-closed': !isOpen })}>
            <div dangerouslySetInnerHTML={{ __html: parsedMarkdown }} />
          </div>
        </div>
      </div>
    </article>
  );
};
