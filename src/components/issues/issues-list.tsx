import React from 'react';
import { useSelector } from 'react-redux';
import { Issue, IssueFilter, MilestoneTab } from '../../models';
import { getAllIssues } from '../../store/selectors';
import { IssueListItem } from './issue-list-item';

interface Props {
  filter: IssueFilter;
  milestone: MilestoneTab | null;
}

export const IssuesList: React.FC<Props> = ({ filter, milestone }) => {
  const issues: Issue[] = useSelector(getAllIssues);
  let issuesToShow: Issue[] = issues;

  if (milestone) {
    if (milestone.label === 'None') {
      issuesToShow = issuesToShow.filter(x => !x.milestone);
    } else {
      issuesToShow = issuesToShow.filter(x => x.milestone?.title === milestone.label);
    }
  }

  switch (filter) {
    case IssueFilter.UPDATE:
      issuesToShow = issuesToShow.filter(x => x.updated_completed === false);
      break;

    case IssueFilter.USER:
      issuesToShow = issuesToShow.filter(x => x.does_not_include_current_user === false);
      break;

    case IssueFilter.ELSE:
      issuesToShow = issuesToShow.filter(x => x.does_not_include_current_user);
      break;
  }

  if (filter === IssueFilter.ALL && issues.length === 0) {
    return (
      <div className="level">
        <p>Add an issue first by entering the issue number in the box above.</p>
      </div>
    );
  } else {
    if (issuesToShow.length === 0) {
      return (
        <div className="level">
          <p>There are no issues for the current filter.</p>
        </div>
      );
    }

    return (
      <div>
        {issuesToShow &&
          issuesToShow.length &&
          issuesToShow.map((x, index) => <IssueListItem issue={x} key={x.id} index={index} />)}
      </div>
    );
  }
};
