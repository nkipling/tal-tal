import cn from 'classnames';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useOutsideClick } from '../../hooks/outside-click';
import { Issue, workflowScope, WorkflowStatus } from '../../models';
import { changeWorkflowLabel } from '../../store/actions/issues';
import { getLabelsLoading } from '../../store/selectors';

type WorkflowStatus = {
  label: string;
  cssClass: string | string[];
  icon: string;
};

function getWorkflowStatus({ labels }: Issue): WorkflowStatus {
  const workflow = labels.find(x => x.includes(workflowScope));

  if (workflow) {
    return {
      label: workflow.replace(workflowScope, ''),
      cssClass: ['is-info', 'is-outlined'],
      icon: 'fa-hourglass-start',
    };
  }

  return {
    label: 'Workflow missing',
    cssClass: 'is-danger',
    icon: 'fa-exclamation-triangle',
  };
}

interface Props {
  issue: Issue;
}

export const WorkflowDisplay: React.FC<Props> = ({ issue }) => {
  const dispatch = useDispatch();
  const ref = useRef(null);
  const { label, cssClass, icon } = getWorkflowStatus(issue);
  const isLoading = useSelector(getLabelsLoading);

  const [isActive, setIsActive] = useState(false);

  const toggleDropdown = () => {
    // It would be nice to dynamically fetch the labels for display in this
    // dropdown but the GitLab API is too restricted here and it's not easily
    // possible right now.

    // if (labelsNeedUpdate) {
    //   dispatch(getProjectLabels(issue.project_id));
    // }

    setIsActive(!isActive);
  };

  useOutsideClick(ref, () => setIsActive(false));

  const onItemClick = (e: React.MouseEvent<HTMLAnchorElement>, x: string) => {
    e.preventDefault();
    setIsActive(false);

    if (x !== label) {
      dispatch(changeWorkflowLabel(issue, `${workflowScope}${x}`));
    }
  };

  return (
    <div ref={ref} className={cn('dropdown', 'control', { 'is-active': isActive })}>
      <div className="dropdown-trigger">
        <button
          className={cn('button', 'is-small', 'has-margin-left-10', cssClass, {
            'is-loading': isLoading,
          })}
          onClick={toggleDropdown}
        >
          <span className="icon is-small">
            <i className={cn('fas', icon)}></i>
          </span>
          <span>{label}</span>
        </button>
      </div>

      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          {WorkflowStatus.map((x, index) => (
            <a
              href="#1"
              className={cn('dropdown-item', { 'is-active': x === label })}
              key={index}
              onClick={e => onItemClick(e, x)}
            >
              {x}
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};
