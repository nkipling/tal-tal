export const asyncTemplate = `## Async Issue Update

#### Status

* __Complete:__ \`50%\`
* __Confidence:__ \`50%\`

#### Notes

<!-- Add any optional notes -->

#### Merge Requests

<!-- Add any related merge request links -->
`;
