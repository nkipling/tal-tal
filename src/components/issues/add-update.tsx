import cn from 'classnames';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Issue, IssueNote } from '../../models';
import { sendAsyncUpdate } from '../../store/actions/issues';
import { getAsyncUpdateLoading } from '../../store/selectors';
import { MarkdownEditor } from '../markdown-editor';
import { asyncTemplate } from './async-template';

interface Props {
  issue: Issue;
  onCancel: () => void;
  lastUpdate: IssueNote | null;
}

export const AddUpdate: React.FC<Props> = ({ onCancel, issue, lastUpdate }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(getAsyncUpdateLoading);
  const [updateValue, setUpdateValue] = useState(lastUpdate?.body || asyncTemplate);

  const onSaveClick = async () => {
    if (!isLoading) {
      await dispatch(sendAsyncUpdate(updateValue, issue));
      onCancelClick();
    }
  };

  const onCancelClick = () => {
    setUpdateValue(asyncTemplate);
    onCancel();
  };

  return (
    <div>
      <MarkdownEditor
        value={updateValue}
        onChange={value => setUpdateValue(value)}
        disabled={isLoading}
      />
      <div className="level has-margin-top-5 is-flex">
        <button
          className={cn('button', 'is-info', 'is-small', { 'is-loading': isLoading })}
          onClick={onSaveClick}
        >
          Save Update
        </button>
        <button className="button is-white is-small" onClick={onCancelClick}>
          Cancel
        </button>
      </div>
    </div>
  );
};
