import cn from 'classnames';
import React from 'react';

interface Props {
  completed?: boolean;
  skipped?: boolean;
}

export const UpdateStatus: React.FC<Props> = ({ completed = false, skipped = false }) => {
  const status = completed
    ? 'Async update completed'
    : skipped
    ? 'Update skipped'
    : 'Async update required';

  const icon = completed
    ? 'fa-check-circle'
    : skipped
    ? 'fa-times-circle'
    : 'fa-exclamation-triangle';

  return (
    <p
      className={cn(
        'level-item',
        { 'has-text-success': completed },
        { 'has-text-danger': !completed && !skipped },
      )}
    >
      <span className="icon is-small">
        <i className={cn('fas', icon)}></i>
      </span>
      <span className="is-size-7 has-text-weight-semibold">&nbsp;{status}</span>
    </p>
  );
};
