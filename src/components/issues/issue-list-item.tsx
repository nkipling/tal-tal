import cn from 'classnames';
import moment from 'moment';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useDragDropItem } from '../../hooks/drag-drop';
import { Issue, IssueNote } from '../../models';
import { issueActions } from '../../store/actions/issues';
import { AppState } from '../../store/reducers';
import { getCurrentUser, getLastAsyncUpdate, isIssueSkipped } from '../../store/selectors';
import { projectUrlFromReference } from '../../utils';
import { InsideRow } from '../inside-row';
import { NotAssigned } from '../not-assigned';
import { AddUpdate } from './add-update';
import { AsyncUpdate } from './async-update';
import { UpdateStatus } from './update-status';
import { WorkflowDisplay } from './workflow-display';

interface Props {
  issue: Issue;
  key: string | number;
  index: number;
}

export const IssueListItem: React.FC<Props> = ({ issue, index }) => {
  const dispatch = useDispatch();
  const ref = useRef<HTMLDivElement>(null);

  const user = useSelector(getCurrentUser);
  const [showUpdateControls, setShowUpdateControls] = useState(false);
  const lastAsyncUpdate = useSelector<AppState, IssueNote | null>(s =>
    getLastAsyncUpdate(s, issue.id),
  );
  const isSkipped = useSelector<AppState, boolean>(s => isIssueSkipped(s, issue.id));

  const opacity = useDragDropItem(index, 'issue', ref);

  const goToProject = () => {
    const url = projectUrlFromReference(issue.references);
    window.open(url, '_blank');
  };

  const removeIssue = () => {
    dispatch(issueActions.removeIssue(issue));
    dispatch(issueActions.buildMilestoneTabs());
  };

  const showControls = () => {
    setShowUpdateControls(!showUpdateControls);
  };

  const skipIssueUpdate = () => {
    dispatch(issueActions.skipIssueUpdate(issue.id));
  };

  return (
    <div
      className={cn('box', { 'is-completed': issue.updated_completed || isSkipped })}
      ref={ref}
      style={{ opacity }}
      draggable={!showUpdateControls}
    >
      <article className="media" key={issue.id}>
        <div className="media-content">
          <div className="content">
            <p style={{ cursor: showUpdateControls ? 'initial' : 'move' }}>
              {issue.updated_completed ? (
                <i className="fas fa-check-circle has-text-success"></i>
              ) : (
                <i className="fas fa-exclamation-triangle has-text-danger"></i>
              )}
              &nbsp;
              <a href={issue.web_url} target="_blank" rel="noopener noreferrer">
                <strong>{issue.title}</strong>
              </a>
              <br />
              <small>{issue.references.full} </small>
              <small>
                opened {moment(issue.created_at).fromNow()} by {issue.author.name}. Last
                updated&nbsp;
                {moment(issue.updated_at).fromNow()}.
              </small>
            </p>
          </div>

          <nav className="level is-mobile has-margin-bottom-0">
            <div className="level-left">
              {issue.milestone && (
                <p className="level-item">
                  <span className="icon is-small">
                    <i className="far fa-clock"></i>
                  </span>
                  <span className="is-size-7 has-text-weight-semibold">
                    &nbsp;{issue.milestone.title}
                  </span>
                </p>
              )}

              <UpdateStatus completed={issue.updated_completed} skipped={isSkipped} />

              {issue.does_not_include_current_user && <NotAssigned />}

              <button className="button is-small is-light" onClick={showControls}>
                <span className="icon is-small">
                  <i className="fas fa-edit"></i>
                </span>
                <span>{showUpdateControls ? 'Stop Updating' : 'Add Update'}</span>
              </button>

              <button
                className="button is-small is-light has-margin-left-10"
                onClick={skipIssueUpdate}
              >
                <span className="icon is-small">
                  <i className="fas fa-forward"></i>
                </span>
                <span>Skip Update</span>
              </button>

              <WorkflowDisplay issue={issue} />
            </div>
          </nav>
          {lastAsyncUpdate && (
            <AsyncUpdate
              note={lastAsyncUpdate}
              url={issue.web_url}
              avatar={lastAsyncUpdate.author.avatar_url}
            />
          )}
          <InsideRow showUpdateControls={showUpdateControls} avatar={user?.avatar_url}>
            <AddUpdate issue={issue} onCancel={showControls} lastUpdate={lastAsyncUpdate} />
          </InsideRow>
        </div>
        <div className="media-right">
          <div className="field has-addons">
            <p className="control">
              <button
                className="button is-white is-small is-primary is-inverted"
                onClick={goToProject}
                data-tooltip="Go to Project"
              >
                <span className="icon is-small">
                  <i className="fas fa-home"></i>
                </span>
              </button>
            </p>
            <p className="control">
              <button
                className="button is-white is-small is-danger is-inverted"
                onClick={removeIssue}
                data-tooltip="Remove"
              >
                <span className="icon is-small">
                  <i className="fas fa-trash"></i>
                </span>
              </button>
            </p>
          </div>
        </div>
      </article>
    </div>
  );
};
