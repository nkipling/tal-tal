import React from 'react';
import { useSelector } from 'react-redux';
import { Route, RouteProps } from 'react-router-dom';
import { getAccessToken } from '../store/selectors';
import { WelcomeView } from '../views/welcome';

export function PrivateRoute({ children, ...rest }: RouteProps): JSX.Element {
  const loggedIn = Boolean(useSelector(getAccessToken));

  return <Route {...rest} render={({ location }) => (loggedIn ? children : <WelcomeView />)} />;
}
