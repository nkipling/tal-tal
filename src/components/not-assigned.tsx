import React from 'react';

export const NotAssigned = () => {
  return (
    <p className="level-item has-text-info">
      <span className="icon is-small">
        <i className="fas fa-info-circle"></i>
      </span>
      <span className="is-size-7 has-text-weight-semibold">
        &nbsp;You are not currently assigned
      </span>
    </p>
  );
};
