import cn from 'classnames';
import React, { KeyboardEvent, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IssueType } from '../models';
import { fetchIssueDetail } from '../store/actions/issues';
import { fetchItemToPin } from '../store/actions/pinned';
import { getIssueLoading } from '../store/selectors';
import { AddPinnedCategory } from './add-pinned-category';
import { AddProject } from './add-project';

interface Props {
  issueType?: IssueType;
  showPinnedType?: boolean;
}

export const AddIssue: React.FC<Props> = ({
  issueType = IssueType.ASYNC,
  showPinnedType = false,
}) => {
  const dispatch = useDispatch();

  const isLoading = useSelector(getIssueLoading);
  const [issueId, setIssueId] = useState('');

  const onKeyPressed = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      addIssue(e);
    }
  };

  const addIssue = (e: any) => {
    e.preventDefault();

    if (issueId) {
      let id = issueId;

      if (issueType === IssueType.ASYNC) {
        if (issueId.charAt(0) === '#') {
          id = issueId.substring(1);
        }

        dispatch(fetchIssueDetail(id));
      } else {
        dispatch(fetchItemToPin(id));
      }
    }

    setIssueId('');
  };

  return (
    <div className="field has-addons is-expanded">
      <AddProject />
      <div className="control is-expanded">
        <input
          className="input"
          type="text"
          placeholder="Issue number"
          value={issueId}
          onChange={e => setIssueId(e.target.value.trim())}
          onKeyPress={onKeyPressed}
        />
      </div>
      {showPinnedType && <AddPinnedCategory />}
      <div className="control">
        <button
          className={cn('button', 'is-info', { 'is-loading': isLoading })}
          onClick={addIssue}
          disabled={!issueId}
        >
          Add Issue
        </button>
      </div>
    </div>
  );
};
