import cn from 'classnames';
import React, { MouseEvent, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useOutsideClick } from '../hooks/outside-click';
import { fetchProjectDetails, issueActions } from '../store/actions/issues';
import { getAllIssueProjects, getProjectLoading, getSelectedProject } from '../store/selectors';

export const AddProject: React.FC = () => {
  const dispatch = useDispatch();
  const ref = useRef(null);
  const projects = useSelector(getAllIssueProjects);
  const selectedProject = useSelector(getSelectedProject);
  const isProjectLoading = useSelector(getProjectLoading);

  const [isActive, setIsActive] = useState(false);
  const [projectId, setProjectId] = useState('');

  const toggleDropdown = () => setIsActive(!isActive);

  useOutsideClick(ref, () => setIsActive(false));

  const onItemClick = (e: MouseEvent<HTMLAnchorElement>, index: number) => {
    e.preventDefault();
    dispatch(issueActions.setSelectedProject(index));
    toggleDropdown();
  };

  const addProjectClick = (e: any) => {
    e.preventDefault();

    if (projectId) {
      dispatch(fetchProjectDetails(projectId));
    }

    setProjectId('');
  };

  return (
    <div ref={ref} className={cn('dropdown', 'control', { 'is-active': isActive })}>
      <div className="dropdown-trigger">
        <button
          className="button"
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          onClick={toggleDropdown}
        >
          <span>{selectedProject?.name || 'Project'}</span>
          <span className="icon is-small">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>

      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          <form onSubmit={addProjectClick}>
            <div className="dropdown-item">
              <div className="field has-addons">
                <div className="control">
                  <input
                    className="input is-small"
                    type="text"
                    placeholder="Project id"
                    value={projectId}
                    onChange={e => setProjectId(e.target.value)}
                  />
                </div>
                <div className="control">
                  <button
                    className={cn('button', 'is-info', ' is-small', {
                      'is-loading': isProjectLoading,
                    })}
                    onClick={addProjectClick}
                    type="submit"
                  >
                    Add
                  </button>
                </div>
              </div>
            </div>
          </form>

          <hr className="dropdown-divider" />

          {projects.map((x, index) => (
            <a
              href="#1"
              className={cn('dropdown-item', { 'is-active': x.selected })}
              key={index}
              onClick={e => onItemClick(e, index)}
            >
              {x.name}
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};
