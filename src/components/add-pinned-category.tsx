import cn from 'classnames';
import React, { MouseEvent, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useOutsideClick } from '../hooks/outside-click';
import { pinnedActions } from '../store/actions/pinned';
import { getPinnedCategories, getSelectedCategory } from '../store/selectors';

export const AddPinnedCategory: React.FC = () => {
  const dispatch = useDispatch();
  const ref = useRef(null);
  const categories = useSelector(getPinnedCategories);
  const selectedCategory = useSelector(getSelectedCategory);

  const [isActive, setIsActive] = useState(false);
  const [categoryName, setCategoryName] = useState('');

  const toggleDropdown = () => setIsActive(!isActive);
  useOutsideClick(ref, () => setIsActive(false));

  useEffect(() => {
    if (categories.length === 0) {
      dispatch(pinnedActions.addCategory('Uncategorised'));
    }
  }, [categories, dispatch]);

  const addCategoryClick = (e: any) => {
    e.preventDefault();

    if (categoryName && categoryName.length < 36) {
      dispatch(pinnedActions.addCategory(categoryName));
    }

    setCategoryName('');
  };

  const onItemClick = (e: MouseEvent<HTMLAnchorElement>, id: string) => {
    e.preventDefault();
    dispatch(pinnedActions.setSelectedCategory(id));
    toggleDropdown();
  };

  return (
    <div ref={ref} className={cn('dropdown', 'control', { 'is-active': isActive })}>
      <div className="dropdown-trigger">
        <button
          className="button"
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          onClick={toggleDropdown}
        >
          <span style={{ maxWidth: 100, minWidth: 100, overflow: 'hidden', textAlign: 'left' }}>
            {selectedCategory?.name || 'Category'}
          </span>
          <span className="icon is-small">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>

      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          <form onSubmit={addCategoryClick}>
            <div className="dropdown-item">
              <div className="field has-addons">
                <div className="control">
                  <input
                    className="input is-small"
                    type="text"
                    placeholder="Category name"
                    value={categoryName}
                    onChange={e => setCategoryName(e.target.value)}
                    maxLength={35}
                  />
                </div>
                <div className="control">
                  <button
                    className={cn('button', 'is-info', ' is-small')}
                    onClick={addCategoryClick}
                    type="submit"
                  >
                    Add
                  </button>
                </div>
              </div>
            </div>
          </form>

          <hr className="dropdown-divider" />

          {categories.map(x => (
            <a
              href="#1"
              className={cn('dropdown-item', { 'is-active': x.selected })}
              key={x.id}
              onClick={e => onItemClick(e, x.id)}
            >
              {x.name}
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};
