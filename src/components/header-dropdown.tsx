import cn from 'classnames';
import React, { MouseEvent, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useOutsideClick } from '../hooks/outside-click';
import { GitLabUser } from '../models';
import { signOut } from '../store/actions/auth';
import { Modal } from './modal';

interface Props {
  user: GitLabUser;
}

export const HeaderDropdown: React.FC<Props> = ({ user }) => {
  const dispatch = useDispatch();
  const ref = useRef(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [showInfoModal, setShowInfoModal] = useState(false);
  const [showLogoutModal, setShowLogoutModal] = useState(false);

  useOutsideClick(ref, () => setDropdownOpen(false));

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const toggleInfoModal = (e: MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    toggleDropdown();
    setShowInfoModal(!showInfoModal);
  };

  const toggleLogoutModal = (e: MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    toggleDropdown();
    setShowLogoutModal(!showLogoutModal);
  };

  const signOutClick = () => {
    dispatch(signOut());
  };

  return (
    <div ref={ref} className={cn('dropdown', 'is-right', { 'is-active': dropdownOpen })}>
      <div className="dropdown-trigger">
        <button
          className="button is-dark"
          aria-haspopup="true"
          aria-controls="dropdown-menu6"
          onClick={toggleDropdown}
        >
          <p className="image is-24x24">
            <img className="is-rounded" src={user?.avatar_url} alt="User Avatar" />
          </p>
          <span className="icon is-small">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu6" role="menu">
        <div className="dropdown-content">
          <div className="dropdown-item">
            <p>
              <strong>{user?.name}</strong>
            </p>
            <p>@{user?.username}</p>
          </div>

          <hr className="dropdown-divider" />

          <a href="#1" className={cn('dropdown-item')} onClick={toggleInfoModal}>
            Wtf is this?
          </a>

          <hr className="dropdown-divider" />

          <a href="#1" className={cn('dropdown-item')} onClick={toggleLogoutModal}>
            Logout
          </a>
        </div>
      </div>

      <Modal
        open={showInfoModal}
        title="Tal Tal 0.0.2"
        buttons={[{ label: 'Okay', classes: ['is-link'], onClick: () => setShowInfoModal(false) }]}
      >
        <p>
          Tal Tal is the result of my boredom and inability to track simple todos. It's designed to
          help me track my Package issues and tell me when the async updates need to be made. It
          also gives me a quick and easy interface to add said updates to the issues.
        </p>

        <h5 className="title is-5 has-margin-bottom-5">How does it work?</h5>
        <p>
          This is a SPA that uses the GitLab API to access any issues you add and uses OAuth for the
          authentication. There is no server or server side proxy so no data is stored or sent
          anywhere. All state (such as issues, notes, etc) is stored locally in an index db. To
          remove all data, simply log out.
        </p>

        <p>
          Use the textbox to add an issue number (with or without the #) and the app will start
          tracking async updates for it. I couldn't get the GitLab API to work too well, so you have
          to select the project that the issue belongs to as well. You can add new projects by
          adding the project id in the dropdown and it will fetch the info for the project.
        </p>

        <h5 className="title is-5 has-margin-bottom-5">I don't trust you at all</h5>
        <p>
          You're right, I'm totally stealing and harvesting all your data and selling it. Please
          don't{' '}
          <a href="https://gitlab.com/nkipling/tal-tal" target="_blank" rel="noopener noreferrer">
            check the source code
          </a>{' '}
          and find out where it's all going (there's probably a good chance some rogue JS package is
          doing this).
        </p>

        <h5 className="title is-5 has-margin-bottom-5">Why Tal Tal?</h5>
        <p>
          Because of the{' '}
          <a href="https://youtu.be/e1shQkN03l8" target="_blank" rel="noopener noreferrer">
            badass music
          </a>
          .
        </p>
      </Modal>

      <Modal
        open={showLogoutModal}
        title="Logout"
        buttons={[
          { label: 'Cancel', onClick: () => setShowLogoutModal(false) },
          { label: 'Logout', classes: ['is-danger'], onClick: signOutClick },
        ]}
      >
        <p>
          Are you sure you wish to logout? All of your data will be erased and you will have to
          enter your issues again.
        </p>
      </Modal>
    </div>
  );
};
