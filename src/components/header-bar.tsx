import cn from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { GitLabUser } from '../models';
import { getCurrentUser } from '../store/selectors';
import { HeaderDropdown } from './header-dropdown';

export const HeaderBar: React.FC = () => {
  const history = useHistory();
  const user = useSelector(getCurrentUser);
  const {
    location: { pathname },
  } = history;

  const onAsyncClick = () => {
    history.push('/');
  };

  const onPinnedClick = () => {
    history.push('pinned');
  };

  return (
    <div className="level is-mobile has-background-primary has-padding-10">
      <div className="level-left">
        <div className="level-item">
          <i className="fas fa-mountain fa-2x has-text-white"></i>
          <p className="title has-text-white has-margin-left-5">Tal Tal</p>
        </div>
      </div>

      <div className="level-right">
        <div className="level-item">
          <button
            className={cn(
              'button',
              'is-dark',
              { 'is-transparent': pathname !== '/' },
              'has-margin-right-10',
            )}
            onClick={onAsyncClick}
          >
            Async
          </button>

          <button
            className={cn(
              'button',
              'is-dark',
              { 'is-transparent': pathname !== '/pinned' },
              'has-margin-right-10',
            )}
            onClick={onPinnedClick}
          >
            Pinned
          </button>

          <HeaderDropdown user={user as GitLabUser} />
        </div>
      </div>
    </div>
  );
};
