import cn from 'classnames';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import shortid from 'shortid';
import { Issue, PinnedNote } from '../../models';
import { pinnedActions } from '../../store/actions/pinned';
import { getAsyncUpdateLoading } from '../../store/selectors';
import { MarkdownEditor } from '../markdown-editor';

interface Props {
  item: Issue;
  onCancel: () => void;
  note?: PinnedNote;
}

export const AddNote: React.FC<Props> = ({ onCancel, item, note }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(getAsyncUpdateLoading);
  const [noteContent, setNoteContent] = useState(note?.content || '');

  const onSaveClick = async () => {
    if (!isLoading) {
      if (note) {
        note.content = noteContent;
        dispatch(pinnedActions.updatePinnedNote({ note, itemId: item.id }));
      } else {
        const newNote = {
          id: shortid.generate(),
          content: noteContent,
        };

        dispatch(pinnedActions.addNoteToPinnedItem({ note: newNote, itemId: item.id }));
      }
      onCancelClick();
    }
  };

  const onCancelClick = () => {
    setNoteContent('');
    onCancel();
  };

  return (
    <div>
      <MarkdownEditor
        value={noteContent}
        onChange={value => setNoteContent(value)}
        disabled={isLoading}
        placeholder="Your note..."
      />
      <div className="level has-margin-top-5 is-flex">
        <button
          className={cn('button', 'is-info', 'is-small', { 'is-loading': isLoading })}
          onClick={onSaveClick}
        >
          Save Note
        </button>
        <button className="button is-white is-small" onClick={onCancelClick}>
          Cancel
        </button>
      </div>
    </div>
  );
};
