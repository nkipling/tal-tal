import cn from 'classnames';
import moment from 'moment';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useDragDropItem } from '../../hooks/drag-drop';
import { PinnedItem as PinnedItemModel, PinnedItemType, PinnedNote } from '../../models';
import { pinnedActions } from '../../store/actions/pinned';
import { getCurrentUser } from '../../store/selectors';
import { projectUrlFromReference, workOutType } from '../../utils';
import { InsideRow } from '../inside-row';
import { NotAssigned } from '../not-assigned';
import { AddNote } from './add-note';
import { PinnedItemNote } from './pinned-item-note';

interface Props {
  item: PinnedItemModel;
  index: number;
}

export const PinnedItem: React.FC<Props> = ({ item, index }) => {
  const dispatch = useDispatch();
  const ref = useRef<HTMLDivElement>(null);

  const user = useSelector(getCurrentUser);
  const [showUpdateControls, setShowUpdateControls] = useState(false);
  const type = workOutType(item.references);

  const opacity = useDragDropItem(index, 'pinned', ref, item.categoryId);

  const goToProject = () => {
    const url = projectUrlFromReference(item.references);
    window.open(url, '_blank');
  };

  const removeItem = () => {
    dispatch(pinnedActions.removePinnedItem(item.id));
  };

  const removeNote = ({ id }: PinnedNote) => {
    dispatch(pinnedActions.removeNoteFromPinnedItem({ noteId: id, itemId: item.id }));
  };

  const showControls = () => {
    setShowUpdateControls(!showUpdateControls);
  };

  return (
    <div
      className={cn('box', { 'is-completed': item.updated_completed })}
      ref={ref}
      style={{ opacity }}
    >
      <article className="media" key={item.id}>
        <div className="media-content">
          <div className="content">
            <p style={{ cursor: 'move' }}>
              <a href={item.web_url} target="_blank" rel="noopener noreferrer">
                <strong>{item.title}</strong>
              </a>
              <br />
              <small>{item.references.full} </small>
              <small>
                opened {moment(item.created_at).fromNow()} by {item.author.name}. Last updated&nbsp;
                {moment(item.updated_at).fromNow()}.
              </small>
            </p>
          </div>

          <nav className="level is-mobile has-margin-bottom-0">
            <div className="level-left">
              <span
                className={cn(
                  'tag',
                  'has-margin-right-5',
                  { 'is-success': type === PinnedItemType.ISSUE },
                  { 'is-warning': type === PinnedItemType.EPIC },
                )}
              >
                {type}
              </span>

              {item.milestone && (
                <p className="level-item">
                  <span className="icon is-small">
                    <i className="far fa-clock"></i>
                  </span>
                  <span className="is-size-7 has-text-weight-semibold">
                    &nbsp;{item.milestone.title}
                  </span>
                </p>
              )}

              {item.does_not_include_current_user && <NotAssigned />}

              <button className="button is-small is-light" onClick={showControls}>
                <span className="icon is-small">
                  <i className="fas fa-edit"></i>
                </span>
                <span>{showUpdateControls ? 'Stop' : 'Add Note'}</span>
              </button>
            </div>
          </nav>

          {item.talUserNotes && item.talUserNotes.length
            ? item.talUserNotes.map(x => (
                <PinnedItemNote item={item} note={x} onDelete={removeNote} key={x.id} />
              ))
            : null}

          <InsideRow showUpdateControls={showUpdateControls} avatar={user?.avatar_url}>
            <AddNote item={item} onCancel={showControls} />
          </InsideRow>
        </div>

        <div className="media-right">
          <div className="field has-addons">
            <p className="control">
              <button
                className="button is-white is-small is-primary is-inverted"
                onClick={goToProject}
                data-tooltip="Go to Project"
              >
                <span className="icon is-small">
                  <i className="fas fa-home"></i>
                </span>
              </button>
            </p>
            <p className="control">
              <button
                className="button is-white is-small is-danger is-inverted"
                onClick={removeItem}
                data-tooltip="Remove"
              >
                <span className="icon is-small">
                  <i className="fas fa-trash"></i>
                </span>
              </button>
            </p>
          </div>
        </div>
      </article>
    </div>
  );
};
