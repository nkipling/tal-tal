import cn from 'classnames';
import React, { useState } from 'react';
import * as Showdown from 'showdown';
import { PinnedItem, PinnedNote } from '../../models';
import { AddNote } from './add-note';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true,
});

interface Props {
  note: PinnedNote;
  item: PinnedItem;
  onDelete: (note: PinnedNote) => void;
}

export const PinnedItemNote: React.FC<Props> = ({ note, onDelete, item }) => {
  const [editing, setEditing] = useState(false);
  const parsedMarkdown = converter.makeHtml(note.content);

  const editNote = () => {
    setEditing(true);
  };

  const removeNote = () => {
    onDelete(note);
  };

  return (
    <article className={cn('media', 'has-margin-top-20', 'inside-row')}>
      <figure className="media-left">
        <i className="fas fa-sticky-note"></i>
      </figure>
      <div className="media-content">
        {editing ? (
          <AddNote item={item} onCancel={() => setEditing(false)} note={note} />
        ) : (
          <div dangerouslySetInnerHTML={{ __html: parsedMarkdown }} className="markdown" />
        )}
      </div>
      <figure className="media-right">
        <button
          className="button is-white is-small is-dark is-inverted"
          onClick={editNote}
          data-tooltip="Edit note"
        >
          <span className="icon is-small">
            <i className="fas fa-edit"></i>
          </span>
        </button>
        <button
          className="button is-white is-small is-danger is-inverted"
          onClick={removeNote}
          data-tooltip="Remove note"
        >
          <span className="icon is-small">
            <i className="fas fa-trash"></i>
          </span>
        </button>
      </figure>
    </article>
  );
};
