import React from 'react';
import { useSelector } from 'react-redux';
import { Category, PinnedItemContainer } from '../../models';
import { getPinnedItems } from '../../store/selectors';
import { PinnedItem } from './pinned-item';

interface Props {
  filter: Category | null;
}

export const PinnedList: React.FC<Props> = ({ filter }) => {
  const categories: PinnedItemContainer[] = useSelector(getPinnedItems);
  let categoriesToShow: PinnedItemContainer[] = categories;

  if (filter) {
    categoriesToShow = categories.filter(x => x.category.id === filter.id);
  }

  if (categories.length === 0) {
    return (
      <div className="level">
        <p>Add an issue first by entering the issue number in the box above.</p>
      </div>
    );
  } else {
    if (categoriesToShow.length === 0) {
      return (
        <div className="level">
          <p>There are no issues for the current category.</p>
        </div>
      );
    }

    return (
      <div>
        {categoriesToShow &&
          categoriesToShow.length &&
          categoriesToShow.map(x => {
            if (filter && filter.id !== x.category.id && x.items.length === 0) {
              return null;
            }

            return (
              <div key={x.category.id}>
                <h5 className="is-size-5 has-text-weight-bold has-margin-top-20 has-margin-bottom-10">
                  {x.category.name}
                </h5>
                {x.items && x.items.length ? (
                  x.items.map((i, index) => <PinnedItem item={i} key={i.id} index={index} />)
                ) : (
                  <p>There are no items.</p>
                )}
              </div>
            );
          })}
      </div>
    );
  }
};
