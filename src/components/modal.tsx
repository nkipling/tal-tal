import cn from 'classnames';
import React from 'react';

type Button = {
  label: string;
  classes?: string[];
  onClick: (e: any) => void;
};

interface Props {
  open?: boolean;
  title: string;
  buttons?: Button[];
}

export const Modal: React.FC<Props> = ({ children, open, title, buttons = [] }) => {
  return (
    <div className={cn('modal-container', { 'modal-is-open': open })}>
      <div className="modal-body panel">
        <p className="panel-heading">{title}</p>
        <div className="panel-block">
          <div className="content">{children}</div>
        </div>
        {buttons && buttons.length > 0 && (
          <div className="panel-block justify-flex-end">
            {buttons.map((x, index) => (
              <button
                className={cn('button', 'is-white', { 'has-margin-left-5': index > 0 }, x.classes)}
                onClick={x.onClick}
                key={index}
              >
                {x.label}
              </button>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};
