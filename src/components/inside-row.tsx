import cn from 'classnames';
import React from 'react';

interface Props {
  showUpdateControls: boolean;
  avatar?: string;
}

export const InsideRow: React.FC<Props> = ({ avatar, showUpdateControls, children }) => {
  return (
    <article
      className={cn(
        'media',
        'has-margin-top-20',
        'inside-row',
        { 'show-controls': showUpdateControls },
        { 'hide-controls': !showUpdateControls },
      )}
    >
      <figure className="media-left">
        <p className="image is-48x48">
          <img className="is-rounded" src={avatar} alt="User Avatar" />
        </p>
      </figure>
      <div className="media-content">
        <div className="content">{children}</div>
      </div>
    </article>
  );
};
