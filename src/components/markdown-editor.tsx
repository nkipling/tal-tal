import React, { useEffect, useState } from 'react';
import ReactMde from 'react-mde';
import 'react-mde/lib/styles/css/react-mde-all.css';
import * as Showdown from 'showdown';
import '../assets/scss/markdown-editor.scss';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true,
});

interface MarkdownEditorProps {
  onChange: (value: string) => void;
  value?: string;
  disabled?: boolean;
  placeholder?: string;
}

export const MarkdownEditor: React.FC<MarkdownEditorProps> = props => {
  const [value, setValue] = React.useState(props.value);
  const [selectedTab, setSelectedTab] = useState<'write' | 'preview'>('write');

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const onChange = (value: string) => {
    setValue(value);

    if (props.onChange) {
      props.onChange(value);
    }
  };

  return (
    <ReactMde
      value={value}
      onChange={onChange}
      selectedTab={selectedTab}
      onTabChange={setSelectedTab}
      minEditorHeight={200}
      minPreviewHeight={140}
      maxEditorHeight={200}
      textAreaProps={{
        placeholder: props.placeholder || 'Comment...',
        disabled: props.disabled || false,
        draggable: false,
      }}
      generateMarkdownPreview={markdown => Promise.resolve(converter.makeHtml(markdown))}
    />
  );
};
