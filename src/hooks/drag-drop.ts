import { DropTargetMonitor, useDrag, useDrop, XYCoord } from 'react-dnd';
import { useDispatch } from 'react-redux';
import { issueActions } from '../store/actions/issues';
import { pinnedActions } from '../store/actions/pinned';

interface DragItem {
  index: number;
  id: string;
  type: string;
}

type Type = 'pinned' | 'issue';

export function useDragDropItem(
  index: number,
  type: Type,
  ref: React.RefObject<HTMLDivElement>,
  categoryId?: string,
) {
  const dispatch = useDispatch();

  const [{ opacity }, drag] = useDrag({
    item: { type, index },
    collect: monitor => ({
      opacity: monitor.isDragging() ? 0.5 : 1,
    }),
  });

  const [, drop] = useDrop({
    accept: type,
    hover(item: DragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current!.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      if (type === 'pinned') {
        dispatch(
          pinnedActions.updateIndexPosition({
            currentIndex: dragIndex,
            newIndex: hoverIndex,
            categoryId,
          }),
        );
      } else {
        dispatch(
          issueActions.updateIndexPosition({ currentIndex: dragIndex, newIndex: hoverIndex }),
        );
      }

      item.index = hoverIndex;
    },
  });

  drag(drop(ref));

  return opacity;
}
